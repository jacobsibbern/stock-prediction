# Stock prediction

Python script to get historic data, calculate and predict stock prices for a company. 

# Flexio Bank System Client
## About
- **Author:** Jacob Sibbern
- **Description:** developed for a school project

## Requirements

1. Running Python environment (tested on python 3.8, 3.9)
2. Private local environment (optional but recommended)
3. Dependancy modules 

#### Create a local environment for this: 

###### Windows
```
py -m venv keras
cd keras\Scripts\
activate
```

###### Linux / Mac OS
```
python3 -m venv kerasenv
cd kerasvenv kerasvenv $ source bin/activate
```

#### Clone repository
```
git clone https://gitlab.com/jacobsibbern/stock-prediction.git
```

#### Or download zip file and unzip
```
https://gitlab.com/jacobsibbern/stock-prediction/-/archive/master/stock-prediction-master.zip
```

###### Update pip in the virtual environment
```
pip install --upgrade pip
```

###### Install modules with pip
```
pip install tensorflow
pip install numpy 
pip install pandas 
pip install pandas_datareader 
pip install matplotlib 
pip install scipy 
pip install sklearn
pip install -U scikit-learn 
pip install seaborninstall -U scikit-learn # Might work without this module even if it errors. 
pip install keras 
```

#### Run app.py within the new environment: 
###### Windows
```
cd "\to\directory"
py app.py
```

###### Linux / Mac OS
```
cd /to/directory/for/repository/
python3 app.py
```

Problems may rise with 3rd party libraries. Only way to solve this is to go through the Keras tutorial on installing the required modules: https://www.tutorialspoint.com/keras/keras_installation.htm
